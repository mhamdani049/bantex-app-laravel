<?php
return [
    'response' => [
        'status' => [
            'error' => 'error',
            'failed' => 'failed',
            'cancel' => 'cancel',
            'success' => 'success'
        ]
    ]
];
