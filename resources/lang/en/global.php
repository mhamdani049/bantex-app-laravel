<?php

return [
    'manageToGetData' => 'Manage to get data',
    'successfullyCreatedNewData' => 'Successfully created new data',
    'successfullyUpdateData' => 'Successfully update data',
    'successfullyDeleteData' => 'Successfully delete data',
    'dataNotFound' => 'Data not found',
    'tokenIsInvalid' => 'Token is Invalid',
    'tokenIsExpired' => 'Token is Expired'
];
