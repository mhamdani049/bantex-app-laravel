<?php

use Illuminate\Database\Seeder;

class MContainerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = ['CONTAINER-001', 'CONTAINER-002', 'CONTAINER-003', 'CONTAINER-004', 'CONTAINER-005'];
        foreach ($datas as $key => $value) {
            DB::table('TB_M_CONTAINER')->insert([
                'ID_M_CONTAINER' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                'NAME' => $value,
                'STATUS' => true,
                'CREATED_BY' => '0',
                'CREATED_AT' => DB::raw('CURRENT_TIMESTAMP')
            ]);
        }
    }
}
