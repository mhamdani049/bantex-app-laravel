<?php

use Illuminate\Database\Seeder;

class MBinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = ['A01', 'A02', 'A03'];
        foreach ($datas as $key => $value) {
            for ($a = 1; $a <= 5; $a++) {
                DB::table('TB_M_BIN')->insert([
                    'ID_M_BIN' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                    'NAME' => $value,
                    'SERIAL_NUMBER' => $a,
                    'STATUS' => true,
                    'CREATED_BY' => '0',
                    'CREATED_AT' => DB::raw('CURRENT_TIMESTAMP')
                ]);
            }
        }
    }
}
