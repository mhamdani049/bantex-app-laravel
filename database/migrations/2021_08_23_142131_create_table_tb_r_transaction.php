<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbRTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_R_TRANSACTION', function (Blueprint $table) {
            $table->string('ID_R_TRANSACTION', 36)->primary();
            $table->string('ID_M_ITEM', 36);
            $table->string('ID_M_BIN', 36);
            $table->integer('QTY');
            $table->enum('SOURCE', ['IN', 'OUT', 'NAVISION']);
            $table->string('CREATED_BY');
            $table->string('UPDATED_BY')->nullable();
            $table->datetime('CREATED_AT');
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_R_TRANSACTION');
    }
}
