<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbTBinHasItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_T_BIN_HAS_ITEM', function (Blueprint $table) {
            $table->string('ID_T_BIN_HAS_ITEM', 36)->primary();
            $table->string('ID_M_BIN', 36);
            $table->string('ID_M_ITEM', 36);
            $table->string('CREATED_BY');
            $table->string('UPDATED_BY')->nullable(); 
            $table->datetime('CREATED_AT');
            $table->datetime('UPDATED_AT')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_T_BIN_HAS_ITEM');
    }
}
