<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbMSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_M_SETTING', function (Blueprint $table) {
            $table->string('ID_M_SETTING', 36)->primary();
            $table->string('KEY');
            $table->longText('VALUE');
            $table->string('CREATED_BY');
            $table->string('UPDATED_BY')->nullable(); 
            $table->datetime('CREATED_AT');
            $table->datetime('UPDATED_AT')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_M_SETTING');
    }
}
