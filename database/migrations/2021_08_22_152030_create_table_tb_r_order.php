<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbROrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_R_ORDER', function (Blueprint $table) {
            $table->string('ID_R_ORDER', 36)->primary();
            $table->string('ID_M_CONTAINER', 36);
            $table->string('ORDER_NO');
            $table->string('SKU');
            $table->integer('ORDER_QTY');
            $table->string('RECIPIENT_NAME');
            $table->boolean('STATUS');
            $table->enum('SOURCE', ['TOKOPEDIA', 'SHOPEE', 'BUKALAPAK', 'LAZADA', 'JDID', 'ZALORA', 'LAINNYA']);
            $table->integer('AVAILABLE_QTY');
            $table->integer('LESS_QTY');
            $table->integer('IS_REQ_LESS_QTY');
            $table->string('RACK_NO');
            $table->string('SHIPPING_NAME');
            $table->string('SHIPPING_COST');
            $table->string('CREATED_BY');
            $table->string('UPDATED_BY')->nullable();
            $table->datetime('CREATED_AT');
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_R_ORDER');
    }
}
