<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbMBin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_M_BIN', function (Blueprint $table) {
            $table->string('ID_M_BIN', 36)->primary();
            $table->string('NAME');
            $table->integer('SERIAL_NUMBER');
            $table->boolean('STATUS');
            $table->string('CREATED_BY');
            $table->string('UPDATED_BY')->nullable();
            $table->datetime('CREATED_AT'); 
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_M_BIN');
    }
}
