<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbMItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_M_ITEM', function (Blueprint $table) {
            $table->string('ID_M_ITEM', 36)->primary();
            $table->string('INV_PG');
            $table->string('ITEM_NO');
            $table->string('DESCRIPTION');
            $table->integer('TOTAL');
            $table->string('UOM');
            $table->string('CREATED_BY'); 
            $table->string('UPDATED_BY')->nullable(); 
            $table->datetime('CREATED_AT'); 
            $table->datetime('UPDATED_AT')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_M_ITEM');
    }
}
