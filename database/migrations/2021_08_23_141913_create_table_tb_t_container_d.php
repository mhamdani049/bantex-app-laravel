<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTbTContainerD extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_T_CONTAINER_D', function (Blueprint $table) {
            $table->string('ID_T_CONTAINER_D', 36)->primary();
            $table->string('ID_T_CONTAINER_H', 36);
            $table->string('ID_M_ITEM', 36);
            $table->integer('SCANNED_QTY');
            $table->string('CREATED_BY'); 
            $table->string('UPDATED_BY')->nullable(); 
            $table->datetime('CREATED_AT'); 
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_T_CONTAINER_D');
    }
}
