<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors', 'jwt.verify', 'audit', 'permission']], function() {
    Route::get('home/welcome', 'HomeController@welcome')->name('api.home.welcome');
    Route::prefix('mbin')->group(function() {
        Route::get('/', 'MBinController@index')->name('api.mbin.index');
        Route::post('/', 'MBinController@store')->name('api.mbin.store');
        Route::put('/{ID_M_BIN}', 'MBinController@update')->name('api.mbin.update');
        Route::delete('/{ID_M_BIN}', 'MBinController@delete')->name('api.mbin.delete');
        Route::get('/{ID_M_BIN}', 'MBinController@detail')->name('api.mbin.detail');
    });
});
