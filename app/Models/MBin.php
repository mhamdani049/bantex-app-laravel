<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Log, Str;

class MBin extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql';
	protected $table = 'TB_M_BIN';
	protected $primaryKey = 'ID_M_BIN';
    protected $keyType = 'string';
    protected $fillable = [
        'ID_M_BIN',
        'NAME',
        'SERIAL_NUMBER',
        'STATUS',
        'CREATED_BY',
        'UPDATED_BY',
        'CREATED_AT',
        'UPDATED_AT',
        'DELETED_AT'
    ];
    protected $hidden = [];
    protected $dates = ['DELETED_AT'];

    public $incrementing = false;

    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    const DELETED_AT = 'DELETED_AT';
}
