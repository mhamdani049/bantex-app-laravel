<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid as Generator;
use Storage, Mail, Lang;
use App\Models\MBin;

class MBinService {
    public function store($request, $user) {
        $requestBody = $request->all();

        try {
            $createMBin = MBin::create($requestBody);
        } catch (Exception $e) {
            return \ResponseHelper::generate(null, $e->getMessage(), "10", config('constant.response.status.error'));
        }

        return \ResponseHelper::generate($createMBin, Lang::get("global.successfullyCreatedNewData"));
    }

    public function update($id, $request, $user) {
        $requestBody = $request->all();
        
        try {
            $data = \BlueprintHelper::findOne($id, MBin::query());
        } catch (Exception $e) {
            return \ResponseHelper::generate(null, $e->getMessage(), "10", config('constant.response.status.error'));
        }

        if (!$data) return \ResponseHelper::generate(null, Lang::get("global.dataNotFound"), "20", config('constant.response.status.error'));

        try {
            $data = tap($data)->update($requestBody);
        } catch (Exception $e) {
            return \ResponseHelper::generate(null, $e->getMessage(), "30", config('constant.response.status.error'));
        }

        return \ResponseHelper::generate($data, Lang::get("global.successfullyUpdateData"));
    }

    public function delete($id, $user) {
        try {
            $data = \BlueprintHelper::findOne($id, MBin::query());
        } catch (Exception $e) {
            return \ResponseHelper::generate(null, $e->getMessage(), "10", config('constant.response.status.error'));
        }

        if (!$data) return \ResponseHelper::generate(null, Lang::get("global.dataNotFound"), "20", config('constant.response.status.error'));

        try {
            $data = tap($data)->delete();
        } catch (Exception $e) {
            return \ResponseHelper::generate(null, $e->getMessage(), "30", config('constant.response.status.error'));
        }

        return \ResponseHelper::generate($data, Lang::get("global.successfullyDeleteData"));
    }
}
