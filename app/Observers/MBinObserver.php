<?php
namespace App\Observers;

use Tymon\JWTAuth\Facades\JWTAuth;
use Ramsey\Uuid\Uuid as Generator;
use Carbon\Carbon;
use App\Models\MBin;

class MBinObserver {
    public function creating(MBin $model) {
        $model->ID_M_BIN = Generator::uuid4()->toString();
        $model->CREATED_BY = JWTAuth::getPayload(JWTAuth::getToken())->toArray()['user'];
        $model->STATUS = true;
    }

    public function updating(MBin $model) {
        $model->UPDATED_AT = Carbon::now()->toDateTimeString();
        $model->UPDATED_BY = JWTAuth::getPayload(JWTAuth::getToken())->toArray()['user'];
    }
}
