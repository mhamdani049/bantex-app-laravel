<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\MBin;
use Lang;

class MBinRepository {
    public function find(Request $request, $user) {
        try {
            $data = \BlueprintHelper::find($request, MBin::query(), $user);
        } catch (\Exception $e) {
            return \ResponseHelper::generate([], $e->getMessage(), "10", config('constant.response.status.error'));
        }

        return \ResponseHelper::generate($data['data'], Lang::get("global.manageToGetData"), "00", config('constant.response.status.success'), $data['metadata']);
    }

    public function findOne($id, $user) {
        $data = \BlueprintHelper::findOne($id, MBin::query());
        return \ResponseHelper::generate($data, Lang::get("global.manageToGetData"), "00", config('constant.response.status.success'));
    }
}
