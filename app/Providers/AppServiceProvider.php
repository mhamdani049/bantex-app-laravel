<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\MBin;
use App\Observers\MBinObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        MBin::observe(MBinObserver::class);
    }
}
