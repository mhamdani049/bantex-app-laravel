<?php
namespace App\Http\Requests\MBin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'NAME' => 'required|max:255',
            'SERIAL_NUMBER' => 'required|numeric|digits_between:1,10'
        ];
    }

    public function attributes() {
        return [
            'NAME' => 'Name',
            'SERIAL_NUMBER' => 'Serial Number'
        ];
    }
}
