<?php
namespace App\Http\Requests\MBin;

use Illuminate\Foundation\Http\FormRequest;

class DeleteRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [];
    }

    public function attributes() {
        return [];
    }
}
