<?php
namespace App\Http\Middleware;

use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Closure;
use JWTAuth;
use Exception;
use Lang;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return \ResponseHelper::generate(null, Lang::get("global.tokenIsInvalid"), "10", config('constant.response.status.error'));
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return \ResponseHelper::generate(null, Lang::get("global.tokenIsExpired"), "20", config('constant.response.status.error'));
            } else {
                return \ResponseHelper::generate(null, $e->getMessage(), "30", config('constant.response.status.error'));
            }
        }
        return $next($request);
    }
}
