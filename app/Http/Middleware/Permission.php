<?php
namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Exception;


class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        $routeArray = app('request')->route()->getAction();
        $controllerAction = class_basename($routeArray['controller']);
        list($controller, $action) = explode('@', $controllerAction);

        $client = new GuzzleClient(['base_uri' => env('APP_URL_SERVICE_SESSION')]);
        $headers = [
            'Authorization' => $request->header('Authorization'),
            'Accept' => 'application/json',
        ];
        $formParams = array(
            'model' => strstr($controller, 'Controller', true),
            'method' => $request->method(),
            'action' => $action
        );
        try {
            $response = $client->post('/permission/check', [
                'headers' => $headers,
                'form_params' => $formParams
            ]);
            $responseJSON = json_decode($response->getBody(), true);
            if ($responseJSON['status'] == 'error') {
                $customResponse = [
                    'status' => $responseJSON['status'],
                    'code' => $responseJSON['code'],
                    'message' => $responseJSON['message'] . ' for ' . json_encode($formParams)
                ];
                return response()->json($customResponse);
            }
            return $next($request);
        } catch (GuzzleException $e) {
            \Sentry\captureException($e);
            return response()->json($e->getMessage());
        }
    }
}
