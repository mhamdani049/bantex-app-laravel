<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\MBinRepository;
use App\Services\MBinService;
use App\Http\Requests\MBin\StoreRequest;
use App\Http\Requests\MBin\UpdateRequest;
use App\Http\Requests\MBin\DeleteRequest;
use Lang;

class MBinController extends Controller {
    protected $repositoryMBin;
    protected $serviceMBin;

    public function __construct(MBinRepository $repositoryMBin, MBinService $serviceMBin) {
        $this->repositoryMBin = $repositoryMBin;
        $this->serviceMBin = $serviceMBin;
    }

    public function index(Request $request) {
        return $this->repositoryMBin->find($request, $this->currentUser());
    }

    public function store(StoreRequest $request) {
        return $this->serviceMBin->store($request, $this->currentUser());
    }

    public function update(UpdateRequest $request) {
        return $this->serviceMBin->update($request->ID_M_BIN, $request, $this->currentUser());
    }

    public function delete(DeleteRequest $request) {
        return $this->serviceMBin->delete($request->ID_M_BIN, $this->currentUser());
    }

    public function detail(Request $request) {
        return $this->repositoryMBin->findOne($request->ID_M_BIN, $this->currentUser());
    }
}
