<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lang;

class HomeController extends Controller
{
    public function welcome(Request $request) {
        activity()->log('Look mum, I logged something');
        $user = $this->currentUser();
        return \ResponseHelper::generate($user, Lang::get("global.manageToGetData"));
    }
}
