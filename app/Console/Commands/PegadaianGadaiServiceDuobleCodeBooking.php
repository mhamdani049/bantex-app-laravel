<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Guzzle\Http\Exception\ClientErrorResponseException;

class PegadaianGadaiServiceDuobleCodeBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pegadaian:gadaiServiceDuobleCodeBooking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'command description PegadaianGadaiServiceDuobleCodeBooking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new GuzzleClient();
        $query          = [];
        $headers        = [
            'Authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjE0MDMiLCJlbWFpbCI6Im1oYW1kYW5pMDQ5QGdtYWlsLmNvbSIsIm5hbWEiOiJKVU5JIFNBUFVUUkEiLCJub19ocCI6IjA4NTc3NDU1MzkyMSIsImFjY2Vzc190b2tlbiI6Ijk3ODE5NTFhMDg2NjUyZWFlNWVjZmQ3NjEyYjYxNTE3ZjRmY2M4MTMxMTlhNDhlZDM4NThmYzU1ZTNlZTQ2N2IyNzMxMzZiNjhmOGM1YWEzOGMzNTVjNzc0ZWE2MzIxMzY2MzhlNTgyZTI2OWMxM2UyYWFkOTQzOWIyYTY2ZDIzYjA1YmFiZTdlYzIzZjc0NWRkMzkxYjM5NGY0NCIsImFnZW4iOiJtb2JpbGUiLCJjaGFubmVsSWQiOiI2MDE3IiwidmVyc2lvbiI6IjMiLCJleHAiOjE2Mjk3OTAzODB9.YlPveHh5QHRyb5AnF-6mQQO4BOR8Hxc27DYLFv5feXU',
            'Accept' => 'application/json'
        ];
        $form_params    = [
            "jenis_perhiasan" => "Cincin",
            "kadar" => "16",
            "berat_kotor" => 4,
            "berat_bersih" => 4,
            "taksiran_atas" => 1960000,
            "taksiran_bawah" => 1470000,
            "kode_outlet" => "12311",
            "waktu_kedatangan" => "15:00",
            "tanggal_kedatangan" => "2021-08-24",
            "jenis_promo" => null,
            "foto" => [],
        ];
        try {
            $response = $client->request('POST', 'https://apidigitaldev.pegadaian.co.id/rc_cbp/gadai/perhiasan', [
                'headers' => $headers,
                'query' => $query,
                'form_params' => $form_params
            ]);
            \Log::info((string) $response->getBody());
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            if ($e->hasResponse()) {
                $error = $e->getResponse();
                \Log::info((string) $error->getBody());
            }
        }

        \Log::info('PegadaianGadaiServiceDuobleCodeBooking send');
        $this->info('PegadaianGadaiServiceDuobleCodeBooking send');
    }
}
