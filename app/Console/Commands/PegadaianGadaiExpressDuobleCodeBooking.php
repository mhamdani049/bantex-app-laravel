<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Guzzle\Http\Exception\ClientErrorResponseException;

class PegadaianGadaiExpressDuobleCodeBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pegadaian:gadaiExpressDuobleCodeBooking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'command description PegadaianGadaiExpressDuobleCodeBooking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $client = new GuzzleClient();
        $query          = [];
        $headers        = [
            'Authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjE0MDYiLCJlbWFpbCI6Im1oYW1kYW5pMDQ5QGdtYWlsLmNvbSIsIm5hbWEiOiJNVUhBTU1BRCBZQU5JUyIsIm5vX2hwIjoiMDg5OTg1MjU4NzgiLCJhY2Nlc3NfdG9rZW4iOiIxNjgyNmI2YzYzNGVjOTZlN2ZlODNmMmQyMGIwZmQyMDAxZGZiNTIzN2NhNmFkNWViMTQ5ZGMyZmEzYWViNzI4NDBiMjE4ZThhYmNhMjEwMmJkNTFmNjMyNTJjZTgxYzhhZDgyMTQ0MTI4NDc2ZWU1MmVjMGM0ZWMyZjAxZGEwZGJjNThhZDg4NjE1OGIyZjMyZjZjYzA1MDkyY2MiLCJhZ2VuIjoibW9iaWxlIiwiY2hhbm5lbElkIjoiNjAxNyIsInZlcnNpb24iOiIzIiwiZXhwIjoxNjI5NzkxMDY5fQ.5QAhE4ncnJo0bR7jeCxoMXjLRbybtk5qTMD-5jGJkmM',
            'Accept' => 'application/json'
        ];
        $form_params    = [
            "taksiran_atas" => "337500",
            "taksiran_bawah" => "75000",
            "kode_outlet" => "12311",
            "waktu_kedatangan" => "10:10",
            "tanggal_kedatangan" => "2021-04-30",
            "jenis_promo" => "undefined",
            "nominal_pengajuan_nasabah" => "330000",
            "destinationAddress" => "Setiabudi 2, Jl. HR Rasuna Said No.62, RT.6/RW.7, Kuningan, Karet Kuningan, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12920, Indonesia",
            "destinationContactName" => "JUMAIYAH",
            "destinationLatLong" => "-6.216463,106.830148",
            "destinationName" => "JUMAIYAH",
            "jarak_to_outlet" => "2.229142083595332",
            "jenis_perhiasan" => [5],
            "berat_bersih" => ["2"],
            "kadar" => ["9"],
            "harga_satuan" => ["375000"],
            "catatan" => ["undefined"]
        ];
        try {
            $response = $client->request('POST', 'https://apidigitaldev.pegadaian.co.id/rc_cbp/gadai/perhiasan_god', [
                'headers' => $headers,
                'query' => $query,
                'form_params' => $form_params
            ]);
            \Log::info((string) $response->getBody());
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            if ($e->hasResponse()) {
                $error = $e->getResponse();
                \Log::info((string) $error->getBody());
            }
        }

        \Log::info('PegadaianGadaiExpressDuobleCodeBooking send');
        $this->info('PegadaianGadaiExpressDuobleCodeBooking send');
    }
}
